import os

import environ
from django.core.management.utils import get_random_secret_key

env = environ.Env()

ENVIRON_VAR_NAME = "DJANGO_DEBUG"


def is_dev_environment():
    """Checks for the ENV enviroment variable and checks whether this is a
    production environment or not
    """
    # DEV_ENVIRONMENT = "DEV"
    # DEV_ENVIRONMENT = "True"
    # debug = os.environ.get(ENVIRON_VAR_NAME, None)

    debug = env.bool(ENVIRON_VAR_NAME, default=False)

    return debug


def is_pythonanywhere():
    """@brief Returns True the web app is hosted in pythonanywhere.com
    or returns None if the right environment variable is not set
    """
    pa_domain_var = "PYTHONANYWHERE_DOMAIN"
    if os.getenv(pa_domain_var):
        return True
    return False


def get_pythonanywhere_hostname():
    """@brief Extracts the hostname from a pythonanywhere env variable"""
    pa_hostname_var = "HOST_NAME"

    # NOTE: What happens if HOST_NAME is not set?
    hostname = os.getenv(pa_hostname_var)
    if hostname:
        return hostname


def get_allowed_hosts():
    """@brief Returns a list of valid hostnames for this Web app"""
    allowed_hosts = []
    # if is_pythonanywhere():
    #     hostname = get_pythonanywhere_hostname()
    #     allowed_hosts.append(hostname)

    allowed_hosts = env.list("DJANGO_ALLOWED_HOSTS", default=[])

    # for host in allowed_hosts:
    #     allowed_hosts.append(host)

    # TODO: Check if this works
    # Filter out all the, possible, None values from the list.

    return list(set(filter(None, allowed_hosts)))


def get_django_secret_key():
    """@brief Retrieves Django's secrete key from the environment or
    generates a new key
    """
    secret_key_var = "DJANGO_SECRET_KEY"

    secret_key = os.getenv(secret_key_var)
    if secret_key:
        return secret_key

    return get_random_secret_key()


def get_db_host():
    """Retrieves the database's hostname"""
    # TODO: Consider storiing the names of these
    # environment variables, inside constants
    # at the top of the script file.
    host = os.environ.get("DB_HOST")
    return host


def get_db_name():
    """Retrieves the database's name"""
    # TODO: Consider storiing the names of these
    # environment variables, inside constants
    # at the top of the script file.
    host = os.environ.get("DB_NAME")
    return host


def get_db_user():
    """Retrieves the database's user"""
    # TODO: Consider storiing the names of these
    # environment variables, inside constants
    # at the top of the script file.
    password = os.environ.get("DB_USER")
    return password


def get_db_pass():
    """Retrieves the database's password"""
    # TODO: Consider storiing the names of these
    # environment variables, inside constants
    # at the top of the script file.
    password = os.environ.get("DB_PASS")
    return password


def use_s3():
    """Decides whether to use an S3 bucket for media and static files"""
    return env.bool("DJANGO_USE_S3", default=False)


def get_s3_access_key_id() -> str:
    """Retrieves the access key id for the user with permission to the S3 bucket"""
    return env("DJANGO_S3_AKID", default="")


def get_s3_secret_access_key() -> str:
    """Retrieves the access key id for the user with permission to the S3 bucket"""
    return env("DJANGO_S3_SAK", default="")


def get_s3_bucket_name() -> str:
    """Retrieves the access key id for the user with permission to the S3 bucket"""
    return env("S3_STORAGE_BUCKET_NAME", default="")


def get_s3_bucket_region() -> str:
    """Retrieves the access key id for the user with permission to the S3 bucket"""
    return env("S3_STORAGE_BUCKET_REGION", default="us-east-1")


def get_static_url() -> str:
    """Retrieves the URL for the static files, this might be a path or a fqdn"""
    return env("DJANGO_STATIC_URL", default="static/")


def get_media_url() -> str:
    """Retrieves the access key id for the user with permission to the S3 bucket"""
    return env("DJANGO_MEDIA_URL", default="media/")


def get_cdn_url() -> str:
    """Retrieves the access key id for the user with permission to the S3 bucket"""
    return env("DJANGO_CDN_URL", default="")
