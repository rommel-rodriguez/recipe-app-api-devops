export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

all: down build up test

build:
	docker compose build

buildprod:
	docker compose -f ./docker-compose.prod.yml build app

rebuild:
	docker compose build --no-cache

rebuildprod:
	docker compose -f ./docker-compose.prod.yml build --no-cache app

up:
	docker compose up -d app

upn:
	docker compose up app


upp:
	docker compose -f ./docker-compose.prod.yml up app

down:
	docker compose down --remove-orphans

startapp:
	docker compose run --rm app sh -c "python manage.py startapp $(app)" 

migrations:
	docker compose run --rm app sh -c "python manage.py makemigrations && python manage.py migrate" 

superuser:
	docker compose run --rm app sh -c "python manage.py createsuperuser" 

test: up
	# docker compose run --rm --no-deps --entrypoint=pytest app /tests/unit /tests/integration /tests/e2e
	docker compose run --rm --no-deps app sh -c 'pytest */tests/unit */tests/integration */tests/e2e'

lint: up
	# docker compose run --rm --no-deps --entrypoint=pytest app /tests/unit /tests/integration /tests/e2e
	docker compose run --rm --no-deps app sh -c 'flake8'

unit-tests: up
	# docker compose run --rm --no-deps --entrypoint=pytest app */tests/unit
	# docker compose run --rm --no-deps app sh -c 'pytest */tests/unit'
	docker compose run --rm --no-deps app sh -c "python manage.py test"

integration-tests: up
	docker compose run --rm --no-deps --entrypoint=pytest app /tests/integration

e2e-tests: up
	# docker compose run --rm --no-deps --entrypoint=pytest app /tests/e2e
	docker compose run --rm --no-deps app sh -c 'pytest */tests/e2e'

logs:
	docker compose logs app | tail -100

collectstatic:
	docker compose run --rm --no-deps app sh -c 'python manage.py collectstatic'

collectprodstatic: buildprod
	docker compose -f ./docker-compose.prod.yml run --build --rm --no-deps app sh -c "python manage.py collectstatic"

# black:
# 	black -l 86 $$(find * -name '*.py')
#####################################################
# Makefile containing shortcut commands for project #
#####################################################

# MACOS USERS:
#  Make should be installed with XCode dev tools.
#  If not, run `xcode-select --install` in Terminal to install.

# WINDOWS USERS:
#  1. Install Chocolately package manager: https://chocolatey.org/
#  2. Open Command Prompt in administrator mode
#  3. Run `choco install make`
#  4. Restart all Git Bash/Terminal windows.

.PHONY: tf-init
tf-init:
	docker-compose -f deploy/docker-compose.yml run --rm terraform init

.PHONY: tf-fmt
tf-fmt:
	docker-compose -f deploy/docker-compose.yml run --rm terraform fmt

.PHONY: tf-validate
tf-validate:
	docker-compose -f deploy/docker-compose.yml run --rm terraform validate

.PHONY: tf-plan
tf-plan:
	docker-compose -f deploy/docker-compose.yml run --rm terraform plan

.PHONY: tf-apply
tf-apply:
	docker-compose -f deploy/docker-compose.yml run --rm terraform apply

.PHONY: tf-destroy
tf-destroy:
	docker-compose -f deploy/docker-compose.yml run --rm terraform destroy

.PHONY: tf-workspace-dev
tf-workspace-dev:
	docker-compose -f deploy/docker-compose.yml run --rm terraform workspace select dev

.PHONY: tf-workspace-staging
tf-workspace-staging:
	docker-compose -f deploy/docker-compose.yml run --rm terraform workspace select staging

.PHONY: tf-workspace-prod
tf-workspace-prod:
	docker-compose -f deploy/docker-compose.yml run --rm terraform workspace select prod

# .PHONY: test
# test:
# 	docker-compose run --rm app sh -c "python manage.py wait_for_db && python manage.py test && flake8"



