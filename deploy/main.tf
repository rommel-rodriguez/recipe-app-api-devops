terraform {
  required_version = "= 0.12.21"
  backend "s3" {
    bucket         = "recipe-app-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}


provider "aws" {
  region = "us-east-1"
  # Version is now deprecated in favor of pinning the version inside 'required_providers'
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Project     = var.project
    Environment = terraform.workspace
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}