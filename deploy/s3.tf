resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-rrp-files"
  # acl           = "public-read"
  force_destroy = true
}

# resource "aws_s3_bucket_public_access_block" "app_public_files" {
#   bucket = aws_s3_bucket.app_public_files.id

#   block_public_acls       = false
#   block_public_policy     = false
#   ignore_public_acls      = false
#   restrict_public_buckets = false
# }

# resource "aws_s3_bucket_policy" "app_public_policy" {
#   bucket = aws_s3_bucket.app_public_files.id

#   policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action    = ["s3:GetObject"],
#         Effect    = "Allow",
#         Resource  = ["${aws_s3_bucket.app_public_files.arn}/*"],
#         Principal = "*"
#       }
#     ]
#   })
# }
