#!/bin/sh

set -e
set -x

python manage.py collectstatic --noinput --verbosity=2

python manage.py wait_for_db
python manage.py migrate

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
